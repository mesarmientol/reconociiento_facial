from __future__ import print_function
import random
import cv2
import numpy as np
from PIL import features
from sklearn.model_selection import train_test_split
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras.optimizers import SGD
from keras.utils import np_utils
from keras.models import load_model
from keras import backend as K
from matplotlib import pyplot
from keras.utils import plot_model, Sequence
from datetime import datetime

import glob
import matplotlib


from pyimagesearch.callbacks.epochcheckpoint import EpochCheckpoint
from pyimagesearch.callbacks.trainingmonitor import TrainingMonitor
from keras.models import model_from_json

import os
from keras.models import load_model

import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D

from datetime import datetime
#from tensorflow.keras import backend as K

from input import extract_data, resize_with_pad, IMAGE_SIZE

#path = r'.\Face Recognition using Keras & Tensorflow'
#fichero para construir y entrenar el modelo con sus respectiuvas epocas
latest_file='nada'
ultima_epoca=0
nb_epoch = 100				#Número de Épocas

save_dir = os.path.join(os.getcwd(), 'saved_models')
model_name = 'model.h5'
try:
    list_of_files = glob.glob('./output/checkpoints/*')
    latest_file = max(list_of_files, key=os.path.getctime)
    ultima_epoca = Numero = len(glob.glob('./output/checkpoints/*.hdf5'))
    #print (latest_file)
#Tome el último entrenamiento y comience desde ese punto
except:
    print(latest_file)
epocas_totales=nb_epoch-ultima_epoca
print(epocas_totales)

logdir = "logs/scalars/" + datetime.now().strftime("%Y%m%d-%H%M%S")


path = r'C:\Users\majin\PycharmProjects\FACIAL2\Face-Recognition-using-Keras---Tensorflow-master\datasetU'


X_train = None
X_valid = None
X_test = None
Y_train = None
Y_valid = None
Y_test = None
IMAGE_SIZE=200
img_rows=IMAGE_SIZE
img_cols=IMAGE_SIZE
img_channels=3
nb_classes=10


images, labels = extract_data(path)
labels = np.reshape(labels, [-1])
print(labels)
# numpy.reshape
X_train, X_test, y_train, y_test = train_test_split(images, labels, test_size=0.3, random_state=random.randint(0, 100))
X_valid, X_test, y_valid, y_test = train_test_split(images, labels, test_size=0.5, random_state=random.randint(0, 100))
if K.image_dim_ordering() == 'th':
    X_train = X_train.reshape(X_train.shape[0], 3, img_rows, img_cols)
    X_valid = X_valid.reshape(X_valid.shape[0], 3, img_rows, img_cols)
    X_test = X_test.reshape(X_test.shape[0], 3, img_rows, img_cols)
    input_shape = (3, img_rows, img_cols)
else:
    X_train = X_train.reshape(X_train.shape[0], img_rows, img_cols, 3)
    X_valid = X_valid.reshape(X_valid.shape[0], img_rows, img_cols, 3)
    X_test = X_test.reshape(X_test.shape[0], img_rows, img_cols, 3)
    input_shape = (img_rows, img_cols, 3)

    # the data, shuffled and split between train and test sets
    #print('X_train shape:', X_train.shape)
    #print(X_train.shape[0], 'train samples')
    #print(X_valid.shape[0], 'valid samples')
    #print(X_test.shape[0], 'test samples')

# convert class vectors to binary class matrices


Y_train = np_utils.to_categorical(y_train, nb_classes,dtype='float32')
Y_valid = np_utils.to_categorical(y_valid, nb_classes,dtype='float32')
Y_test = np_utils.to_categorical(labels, nb_classes, dtype='float32')




X_train = X_train.astype('float32')
X_valid = X_valid.astype('float32')
X_test = X_test.astype('float32')
X_train /= 255
X_valid /= 255
X_test /= 255


X_train = X_train
X_valid = X_valid
X_test = X_test
Y_train = Y_train
Y_valid = Y_valid
Y_test = Y_test



FILE_PATH = r'C:\Users\majin\PycharmProjects\FACIAL2\Face-Recognition-using-Keras---Tensorflow-master\model.h5'


model = None


if latest_file is 'nada':
    logdir = "logs/scalars/" + datetime.now().strftime("%Y%m%d-%H%M%S")
    tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir)
    # Compilamos el modelo desde el inicio

    print("[INFO] compilando modelo...")
    #  Definimos el modélo
    model = Sequential()

    model.add(Convolution2D(32, 3, 3, border_mode='same', input_shape=X_train.shape[1:]))
    model.add(Activation('relu'))
    model.add(Convolution2D(32, 3, 3))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Convolution2D(64, 3, 3, border_mode='same'))
    model.add(Activation('relu'))
    model.add(Convolution2D(64, 3, 3))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())
    #numero de neuronas, activacion relu
    model.add(Dense(512))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))
    model.add(Dense(nb_classes))
    model.add(Activation('softmax'))
    model.summary()
# de lo contrario, estamos utilizando un modelo de punto de control
else:
    # cargar el punto de control desde el disco
    print(latest_file)
    model = load_model(latest_file)

    print("[INFO] old learning rate: {}".format(
        K.get_value(model.optimizer.lr)))

    # se cambia el optimizador tomar en ceunta que aqui piede ir otrio según la necesidad o el mismo,ç
    # en este caso se cambio para mejorar ;la taza de aprenidaze (1e-2) antes se usaba en 1e-2
    K.set_value(model.optimizer.lr, 1e-6)
    print("[INFO] new learning rate: {}".format(
        K.get_value(model.optimizer.lr)))

    # construir el camino hacia la trama de

batch_size = 32
data_augmentation=True


# let's train the model using SGD + momentum (how original).
sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
model.compile(loss='categorical_crossentropy',
                   optimizer=sgd,
                   metrics=['mse', 'acc'])

#ACC colocar
if not data_augmentation:
    print('Not using data augmentation.')
    model.fit(X_train, Y_train,
                   batch_size=batch_size,
                   nb_epoch=nb_epoch,
                   validation_data=(X_valid, Y_valid),
                   shuffle=True)
else:
    print('Using real-time data augmentation.')

    # esto hará el preprocesamiento y el aumento de datos en tiempo real
    datagen = ImageDataGenerator(
        featurewise_center=False,             # establecer la media de entrada a 0 sobre el conjunto de datos
        samplewise_center=False,              # establecer cada muestra media a 0
        featurewise_std_normalization=False,  # dividir entradas por estándar del conjunto de datos
        samplewise_std_normalization=False,   # dividir cada entrada por su estándar
        zca_whitening=False,                  #aplicar blanqueamiento ZCA
        rotation_range=20,                     # rotar imágenes al azar en el rango (grados, 0 a 180)
        width_shift_range=0.2,                # cambiar aleatoriamente las imágenes horizontalmente (fracción del ancho total)
        height_shift_range=0.2,               # mover imágenes al azar verticalmente (fracción de la altura total)
        horizontal_flip=True,                 # voltear imágenes al azar
        vertical_flip=False)                  # voltear imágenes al azar

    # Calcular las cantidades requeridas para la normalización de características
    #(componentes estándar, medio y principal si se aplica blanqueamiento ZCA)
    datagen.fit(X_train)
    callbacks = [
        EpochCheckpoint('./output/checkpoints', every=1,
                        startAt=ultima_epoca), keras.callbacks.TensorBoard(log_dir=logdir)]

    # ajustar el modelo en los lotes generados por datagen.flow()
    history=model.fit_generator(datagen.flow(X_train, Y_train,
                                          batch_size=batch_size),
                             samples_per_epoch=X_train.shape[1],
                             nb_epoch=nb_epoch, callbacks = callbacks,
                             validation_data=(X_valid, Y_valid))
    #pyplot.plot(history.history['mean_squared_error'])
    #pyplot.plot(history.history['mean_absolute_error'])
    #pyplot.plot(history.history['mean_absolute_percentage_error'])
    #pyplot.plot(history.history['cosine_proximity'])
    #pyplot.show()
import os
import tensorflow as tf
from tensorflow.contrib.tensorboard.plugins import projector

metadatap = os.path.join(logdir,'metadata.tsv')
images = tf.Variable((Y_test),name='images')
num=0
nombre=''
with open(metadatap,'w') as metadata_file:
    for row in labels:
        if row == 0:
           nombre = 'bryan'
        if row == 1:
           nombre = 'fabian'
        if row == 2:
           nombre = 'gary'
        if row == 3:
           nombre = 'giss'
        if row == 4:
           nombre = 'grace'
        if row == 5:
           nombre = 'jarol'
        if row == 6:
           nombre = 'javier'
        if row == 7:
           nombre = 'juank'
        if row == 8:
           nombre = 'michael'
        if row == 9:
           nombre = 'pame'
        #c = np.nonzero(Y_test[::1])[1:][0][row]
        #metadata_file.write('{}\n'.format(c))
        metadata_file.write('%s\n'%nombre)
with tf.Session() as sess:
    saver = tf.train.Saver([images])

    sess.run(images.initializer)
    saver.save(sess, os.path.join(logdir,'images.ckpt'))

    config = projector.ProjectorConfig()
    embedding = config.embeddings.add()
    embedding.tensor_name = images.name
    embedding.metadata_path = os.path.basename(metadatap)
    projector.visualize_embeddings(tf.summary.FileWriter(logdir),config)

model.save(FILE_PATH)

print('Model Saved.')

#score = model.evaluate(X_test, Y_test, verbose=0)
#print("%s: %.2f%%" % (model.metrics_names[1], score[1] * 100))
''' 
if __name__ == '__main__':
    dataset = Dataset()
    dataset.read()

    model = Model()
    model.build_model(dataset)
    model.train(dataset, nb_epoch=15)
    model.save()

    #model = Model()
    #model.load()
    #model.evaluate(dataset)
'''