import os

import numpy as np
import cv2

images = []
labels = []

path = r'C:\Users\majin\PycharmProjects\FACIAL2\Face-Recognition-using-Keras---Tensorflow-master\datasetU'
IMAGE_SIZE = 200
def resize_with_pad(image, height=IMAGE_SIZE, width=IMAGE_SIZE):

    def get_padding_size(image):
        h, w, _ = image.shape
        longest_edge = max(h, w)
        top, bottom, left, right = (0, 0, 0, 0)
        if h < longest_edge:
            dh = longest_edge - h
            top = dh // 2
            bottom = dh - top
        elif w < longest_edge:
            dw = longest_edge - w
            left = dw // 2
            right = dw - left
        else:
            pass
        return top, bottom, left, right

    top, bottom, left, right = get_padding_size(image)
    BLACK = [0, 0, 0]
    constant = cv2.copyMakeBorder(image, top , bottom, left, right, cv2.BORDER_CONSTANT, value=BLACK)

    resized_image = cv2.resize(constant, (height, width))
    # cv2.imshow('Face 2',resized_image)
    # cv2.waitKey(500)
    return resized_image

def traverse_dir(path):
    for file_or_dir in os.listdir(path):
        abs_path = os.path.abspath(os.path.join(path, file_or_dir))
        # print(abs_path)
        if os.path.isdir(abs_path):  # dir
            traverse_dir(abs_path)
        else:                        # file
            if file_or_dir.endswith('.jpg'):
                image = read_image(abs_path)
                images.append(image)
                labels.append(file_or_dir)
                #print(labels)
                #print(len(labels))
    return images, labels

def read_image(file_path):
    image = cv2.imread(file_path)
    image = resize_with_pad(image, IMAGE_SIZE, IMAGE_SIZE)

    return image


def extract_data(path):
    images, labels = traverse_dir(path)
    images = np.array(images)
    labels = np.array([
        0 if 'bryan' in label
        else 1 if 'fabian' in label
        else 2 if 'gary' in label
        else 3 if 'giss' in label
        else 4 if 'grace' in label
        else 5 if 'jarol' in label
        else 6 if 'javier' in label
        else 7 if 'juank' in label
        else 8 if 'michael' in label
        else 9 if 'pame' in label
        else 10  for label in labels])
    #print(images[[0]])
    #print(images.shape)
    #print(labels)
    #print(labels.shape)
    return images, labels

data =extract_data(path)
#print(data)