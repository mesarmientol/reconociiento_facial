import random

import cv2
from keras import backend as K
from keras.models import load_model

import os
import cv2
import dlib
import scipy.misc
#Fichero para probar el modelo entrenado
face_detector = dlib.get_frontal_face_detector()

shape_predictor = dlib.shape_predictor('shape_predictor_68_face_landmarks.dat')

workDirectory = os.path.dirname(__file__)

trainDirectory = os.path.join(workDirectory,'images')

celebrityTrainFolder = [os.path.join(trainDirectory, f) for f in os.listdir(trainDirectory)]

count = 0

def rect_to_bb(rect):
    # take a bounding predicted by dlib and convert it
    # to the format (x, y, w, h) as we would normally do
    # with OpenCV
    x = rect.left()
    y = rect.top()
    w = rect.right() - x
    h = rect.bottom() - y

    # return a tuple of (x, y, w, h)
    return (x, y, w, h)

def get_face_detect(imagePath):
    print(count, imagePath)
    image = scipy.misc.imread(imagePath)

    # The 1 in the second argument indicates that we should upsample the image
    # 1 time.  This will make everything bigger and allow us to detect more
    # faces.
    detected_faces = face_detector(image, 1)
    shapes_faces = [shape_predictor(image, face) for face in detected_faces]
    for face_pose in shapes_faces:
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        (x, y, w, h) = rect_to_bb(face_pose.rect)
        image = cv2.rectangle(gray, (x, y), (x + w, y + h), (255, 0, 0), 2)
        crop_img = image[y:y + h, x:x + w]
        #if (v != 0 for v in (x,y,w,h)):
        crop_img = cv2.resize(crop_img, (200, 200), interpolation=cv2.INTER_CUBIC)
        return crop_img
import numpy as np
from keras.utils import to_categorical


IMAGE_SIZE=200
def resize_with_pad(image, height=IMAGE_SIZE, width=IMAGE_SIZE):

    def get_padding_size(image):
        h, w, _ = image.shape
        longest_edge = max(h, w)
        top, bottom, left, right = (0, 0, 0, 0)
        if h < longest_edge:
            dh = longest_edge - h
            top = dh // 2
            bottom = dh - top
        elif w < longest_edge:
            dw = longest_edge - w
            left = dw // 2
            right = dw - left
        else:
            pass
        return top, bottom, left, right

    top, bottom, left, right = get_padding_size(image)
    BLACK = [0, 0, 0]
    constant = cv2.copyMakeBorder(image, top , bottom, left, right, cv2.BORDER_CONSTANT, value=BLACK)

    resized_image = cv2.resize(constant, (height, width))
    # cv2.imshow('Face 2',resized_image)
    # cv2.waitKey(500)
    return resized_image




#ruta = ('./datasetU/grace/grace_1.jpg')

def inicioPrueba(ruta):
    model = load_model('./model.h5')

    image = cv2.imread(ruta)
   # image = get_face_detect(ruta)
   # image = get_face_detect(image)
    if K.image_dim_ordering() == 'th' and image.shape != (1, 3, IMAGE_SIZE, IMAGE_SIZE):
        image = resize_with_pad(image)
        image = image.reshape((1, 3, IMAGE_SIZE, IMAGE_SIZE))
    elif K.image_dim_ordering() == 'tf' and image.shape != (1, 3, IMAGE_SIZE, IMAGE_SIZE):
        image = resize_with_pad(image)
        image = image.reshape((1, IMAGE_SIZE, IMAGE_SIZE, 3))
    image = image.astype('float32')
    image /= 255

    etiquetas = {"bryan": 0, "fabian": 1, "gary": 2, "giss": 3, "grace": 4, "jarol": 5, "javier": 6, "juank": 7,
                 "michael": 8, "pame": 9}

    t = 0

    result = model.predict_classes(image)
    print(result)
    resultado = model.predict(image)
    print(resultado)
    todo = []
    resu = []
    i = 0

    for label in etiquetas:
        print("\t%s ==> %f" % (label, resultado[t][i] ))
        resu.append(resultado[t][i])

        todo.append([label,100*resultado[t][i]])
        i = i + 1







    # la variable todo almacena el porcentale con la etiqueta






    return todo

import numpy as np
import os.path


class IdentityMetadata():
    def _init_(self, base, name, file):
        # dataset base directory
        self.base = base
        # identity name
        self.name = name
        # image file name
        self.file = file

    def _repr_(self):
        return self.image_path()

    def image_path(self):
        return os.path.join(self.base, self.name, self.file)


def load_metadata(path):
    metadata = []
    for i in sorted(os.listdir(path)):
        for f in sorted(os.listdir(os.path.join(path, i))):
            # Check file extension. Allow only jpg/jpeg' files.
            ext = os.path.splitext(f)[1]
            if ext == '.jpg' or ext == '.jpeg':
                metadata.append(IdentityMetadata(path, i, f))
    return np.array(metadata)






